package main

import(
	"fmt"
	"strconv"
	"container/list"
)

type stctemplate struct {
	partNumber int
	jjidx int
	name string
}

type servicepack struct {
	name string
	semver string
	dbconnect string
}

type vrune struct {
	name string
	version string
	author string
	componentList *list.List
}



type vnet interface {
	connectDB() error
	resetDB() error
	getIndices() int
}

func (s *stctemplate) overview() string {
	r :=strconv.FormatInt(int64(s.jjidx), 10)
	return s.name + ", " + r
}

func (v * servicepack) overview() string {
	return " <> " + v.name + " | " + v.semver + " | " + v.dbconnect
}
/*

	microservice functionality is in the main function


 */
func main() {
	fmt.Println("STCMS service endpoint")
	testconstruct := stctemplate {
		partNumber: 101,
		jjidx:      9,
		name:       "VayenthaNet",
	}

	tsp := servicepack{
		name:      "VNet alpha",
		semver:    "1.1.0",
		dbconnect: "192.168.1.1",
	}

	rn := vrune{
		name:          "test rune",
		version:       "1.0.0",
		author:        "",
		componentList: list.New(),
	}
	//gox := list.Element{Value:"hello"}
	rn.componentList.PushBack("hello")
	rn.componentList.PushBack("world")
	fmt.Printf(" component >>  %+v \n", testconstruct)
	ov := testconstruct.overview()
	sp := tsp.overview()
	fmt.Println(ov)
	fmt.Println(sp)
	for e:= rn.componentList.Front(); e != nil; e=e.Next() {
		fmt.Println(e.Value)
	}
}